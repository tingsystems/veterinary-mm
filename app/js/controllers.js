(function () {
    'use strict';

    function ItemCtrl(ItemSrv, $localStorage, $rootScope, $filter, NotificationSrv) {
        var self = this;
        $rootScope.items = $localStorage.items ? $localStorage.items : [];
        $rootScope.total = $localStorage.total;
        $rootScope.shipmentPrice = $localStorage.shipmentPrice;
        self.list = [];
        self.page = 0;
        self.next = true;
        self.busy = false;

        self.getMoreItems = function () {
            if (self.busy || !self.next)return;
            self.page += 1;
            self.busy = true;

            ItemSrv.get({
                isActive: 'True',
                pageSize: 10,
                ordering: '-createdAt',
                page: self.page
            }).$promise.then(function (results) {
                    //get all items
                    self.list = self.list.concat(results.results);
                    // filter to delete mmsharepays
                    self.filter = $filter('filter')(self.list, {kind: "mmsharepay"});
                    //compare two list and remove duplicates
                    //http://stackoverflow.com/questions/14930516/compare-two-javascript-arrays-and-remove-duplicates
                    self.list = self.list.filter(function (val) {
                        return self.filter.indexOf(val) == -1;
                    });
                    self.busy = false;
                    self.next = results.next;
                });
        };

        $localStorage.items = $rootScope.items;
        $localStorage.total = $rootScope.total;
        $localStorage.shipmentPrice = $rootScope.shipmentPrice;

        self.clearCart = function () {
            $rootScope.items = [];
            $rootScope.total = 0;
            $rootScope.shipmentPrice = 0;
            $localStorage.items = [];
            $localStorage.total = 0;
            $localStorage.shipmentPrice = 0;
        };

        self.itemInCart = function (item) {
            var find_item = $filter('filter')($rootScope.items, {id: item.id})[0];
            return !!find_item;
        };

        // we calculate the total from items on the cart
        var getTotal = function () {
            $rootScope.total = 0;
            $rootScope.shipmentPrice = 0;
            angular.forEach($rootScope.items, function (value, key) {
                $rootScope.total += parseFloat(value.price) * value.qty;
                if (value.shipmentPrice != null) {
                    $rootScope.total += parseFloat(value.shipmentPrice);
                    $rootScope.shipmentPrice += parseFloat(value.shipmentPrice);

                }
            });

            $localStorage.total = $rootScope.total;
            $localStorage.shipmentPrice = $rootScope.shipmentPrice;
        };

        getTotal();
        self.setItem = function (item, qty) {
            var find_item = $filter('filter')($rootScope.items, {id: item.id})[0];
            if (find_item) {
                if (qty < 1) {
                    // Remove item from cart
                    $rootScope.items.splice([$rootScope.items.indexOf(find_item)], 1);
                } else {
                    if (qty) {
                        $rootScope.items[$rootScope.items.indexOf(find_item)].qty = qty;
                        NotificationSrv.success(item.name + 'agregado al carrito');
                    }
                }
            } else {
                $rootScope.items.push(item);
                NotificationSrv.success(item.name + ' agregado al carrito');
            }
            getTotal();
        };

        self.getMoreItems();
    }

    function ItemDetailCtrl(ItemSrv, $stateParams, $localStorage, $rootScope, NotificationSrv, $filter, $state) {
        var self = this;
        self.detail = {};
        $rootScope.items = $localStorage.items ? $localStorage.items : [];
        $rootScope.total = $localStorage.total;
        $rootScope.shipmentPrice = $localStorage.shipmentPrice;

        self.busy = true;
        ItemSrv.get({slug: $stateParams.slug}).$promise.then(function (results) {
            self.detail = results;
            self.busy = false;
        });

        $localStorage.items = $rootScope.items;
        $localStorage.total = $rootScope.total;
        $localStorage.shipmentPrice = $rootScope.shipmentPrice;

        // we calculate the total from items on the cart
        var getTotal = function () {
            $rootScope.total = 0;
            $rootScope.shipmentPrice = 0;
            angular.forEach($rootScope.items, function (value, key) {
                $rootScope.total += parseFloat(value.price) * value.qty;
                if (value.shipmentPrice != null) {
                    $rootScope.total += parseFloat(value.shipmentPrice);
                    $rootScope.shipmentPrice += parseFloat(value.shipmentPrice);
                }
            });
            $localStorage.total = $rootScope.total;
            $localStorage.shipmentPrice = $rootScope.shipmentPrice;
        };

        getTotal();
        self.setItem = function (item, qty, nextUrl) {
            var find_item = $filter('filter')($rootScope.items, {id: item.id})[0];
            if (find_item) {
                if (qty < 1) {
                    // Remove item from cart
                    $rootScope.items.splice([$rootScope.items.indexOf(find_item)], 1)
                } else {
                    if (qty) {
                        $rootScope.items[$rootScope.items.indexOf(find_item)].qty = qty;
                        NotificationSrv.success(item.name + 'agrecado al carrito');
                    }
                }
            } else {
                $rootScope.items.push(item);
                NotificationSrv.success(item.name + ' agregado al carrito');
                if (nextUrl)
                    $state.go(nextUrl);
            }
            getTotal();
        };
    }

    function CartCtrl(ChargeSrv, $localStorage, $rootScope, NotificationSrv, $filter, $state, $q) {
        // this is scope
        var self = this;
        $rootScope.items = $localStorage.items ? $localStorage.items : [];
        $rootScope.total = $localStorage.total;
        $rootScope.shipmentPrice = $localStorage.shipmentPrice;

        if (!$rootScope.items.length) {
            NotificationSrv.error('Tu carrito esta vacio');
            $state.go('home');
        }

        self.busyCard = false;
        self.total = $localStorage.total;
        self.charge = {};
        self.payment = {
            'card': {
                'number': null,
                'name': '',
                'exp_year': '',
                'exp_month': '',
                'cvc': null
            }
        };

        var publishKey = 'key_c2rdgosQm6hBrsQnzq8bNVw';
        var setPublishableKey = function () {
            Conekta.setLanguage('es');
            Conekta.setPublishableKey(publishKey);
        };

        var initialGateway = function () {
            var deferred = $q.defer();
            $.getScript("https://conektaapi.s3.amazonaws.com/v0.3.2/js/conekta.js")
                .done(function (script, textStatus) {
                    deferred.promise.then(function (response) {
                        // Conekta Public Key
                        setPublishableKey();
                    });
                    deferred.resolve(textStatus);
                })
                .fail(function (jqxhr, settings, exception) {
                    console.log("Triggered ajaxError handler.");
                });
        };

        initialGateway();
        var successResponseHandler = function (token) {
            var params = {
                brand: Conekta.card.getBrand(self.payment.card.number),
                cardholder: self.payment.card.name,
                authCode: token.id,
                phone: self.payment.phone,
                email: self.payment.email,
                items: $rootScope.items,
                amount: $rootScope.total,
                shop: $rootScope.shopId,
                kind: 'card_payment'
            };
            ChargeSrv.save(params).$promise.then(function (response) {
                NotificationSrv.success('Compra procesada correctamente!');
                $rootScope.items = [];
                $rootScope.total = 0;
                $localStorage.items = [];
                $localStorage.total = 0;
                $localStorage.shipmentPrice = 0;
                $rootScope.purchaseDetail = response;
                $state.go('success_paid');
            }, function (data) {
                self.busyCard = false;
                angular.forEach(data.data, function (value, key) {
                    NotificationSrv.error(value);
                    $rootScope.items = [];
                    $rootScope.total = 0;
                    $localStorage.items = [];
                    $localStorage.total = 0;
                    $localStorage.shipmentPrice = 0;
                    $state.go('home');
                });
            });
        };

        // we calculate the total from items on the cart
        var getTotal = function () {
            $rootScope.total = 0;
            $rootScope.shipmentPrice = 0;
            angular.forEach($rootScope.items, function (value, key) {
                $rootScope.total += parseFloat(value.price) * value.qty;
                if (value.shipmentPrice != null) {
                    $rootScope.total += parseFloat(value.shipmentPrice);
                    $rootScope.shipmentPrice += parseFloat(value.shipmentPrice);
                }
            });
            $localStorage.total = $rootScope.total;
            $localStorage.shipmentPrice = $rootScope.shipmentPrice;
        };

        getTotal();
        self.setItem = function (item, qty, nextUrl) {
            var find_item = $filter('filter')($rootScope.items, {id: item.id})[0];
            if (find_item) {
                if (qty < 1) {
                    // Remove item from cart
                    $rootScope.items.splice([$rootScope.items.indexOf(find_item)], 1);
                    $rootScope.shipmentPrice -= item.shipmentPrice;
                    if (!$rootScope.items.length)
                        $state.go('home');
                } else {
                    if (qty)
                        $rootScope.items[$rootScope.items.indexOf(find_item)].qty = qty;

                }
            } else {
                $rootScope.items.push(item);
                NotificationSrv.success(item.name + ' agregado al carrito');
                if (nextUrl)
                    $state.go(nextUrl);
            }
            getTotal();
        };

        var errorResponseHandler = function (error) {
            var deferred = $q.defer();
            deferred.promise.then(function (error) {
                self.busyCard = false;
                NotificationSrv.error(error.message_to_purchaser);
            });
            deferred.resolve(error);
        };

        self.processPayment = function () {
            self.busyCard = true;
            Conekta.token.create(self.payment, successResponseHandler, errorResponseHandler);
        };

        self.clearCart = function () {
            $rootScope.items = [];
            $rootScope.total = 0;
            $localStorage.items = [];
            $localStorage.total = 0;
            $localStorage.shipmentPrice = 0;
            $state.go('home');
        };
    }

    // create the module and assign controllers
    angular.module('ts.controllers', ['ts.services'])
        .controller('ItemCtrl', ItemCtrl)
        .controller('ItemDetailCtrl', ItemDetailCtrl)
        .controller('CartCtrl', CartCtrl);

    // inject dependencies to controllers
    ItemCtrl.$inject = ['ItemSrv', '$localStorage', '$rootScope', '$filter', 'NotificationSrv'];
    ItemDetailCtrl.$inject = ['ItemSrv', '$stateParams', '$localStorage', '$rootScope', 'NotificationSrv', '$filter', '$state'];
    CartCtrl.$inject = ['ChargeSrv', '$localStorage', '$rootScope', 'NotificationSrv', '$filter', '$state', '$q'];
})();